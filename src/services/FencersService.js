import axios from "axios";

export class FencersService {
    constructor() {
        this.url = 'http://localhost:8000/api-fencing/fencers/'
    }
    async getAll() {
        let fencers = await axios.get(this.url);
        return fencers.data;
    }
    async getOne(routeParam) {
        let fencer = await axios.get(this.url + routeParam);
        return fencer.data;
    }
}